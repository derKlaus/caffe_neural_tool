/*
 *  delaunay.h
 *  aamlib-opencv
 *
 *  Created by Chen Xing on 10-2-12.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#include "opencv2/opencv.hpp"
//#include "cv.h"
//#include "opencv2/legacy/legacy.hpp"
#include <algorithm>
#include <cstdlib>
#include <map>
#include <set>
#include <vector>
using cv::Point_;
using cv::Mat_;
using std::vector;
using std::set;
using std::map;
using std::sort;

class Delaunay;

struct Triangle {
    Point_<int> v[3];
};

struct TriangleInID {
    int v[3];
};


int pComp(const void *p1, const void *p2);

//! Find the Delaunay division for given points(Return in int coordinates).
template <class T>
vector<Triangle> delaunayDiv(const vector<Point_<T> > &vP, cv::Rect boundRect) {

    //vp -> punkte die eingefügt wrden sollen
    cv::Subdiv2D subdiv(boundRect);
    for(size_t e = 0; e < vP.size(); e++)
    {
        subdiv.insert(vP[e]);
    }

    vector<cv::Vec6f> triangles;
    subdiv.getTriangleList(triangles);
    //Vec6f:
    //p1_x = v[0], p1_y = v[1],
    //p2_x = v[2]...

    vector<Triangle> tRet;
    tRet.resize(triangles.size());

    for(size_t i = 0; i < triangles.size(); i++)
    {
        cv::Vec6f t = triangles[i];
        tRet[i].v[0] =  cv::Point(cvRound(t[0]), cvRound(t[1]));
        tRet[i].v[1] =  cv::Point(cvRound(t[2]), cvRound(t[3]));
        tRet[i].v[2] =  cv::Point(cvRound(t[4]), cvRound(t[5]));
    }


   return tRet;
}

template <class T>
struct PointLess {
    bool operator()(const Point_<T> &pa, const Point_<T> &pb) const {
        return (pa.x < pb.x) || (pa.x == pb.x && pa.y < pb.y);
    }
};

bool operator<(const TriangleInID &a, const TriangleInID &b);


template <typename T>
void labelMatByTriInID(const vector<Point_<T> > &vP,
                       vector<TriangleInID> &triList, Mat_<int> &mapMat,
                       cv::Size labelSize) {
    mapMat.create(labelSize);
    mapMat.setTo(triList.size());

    vector<TriangleInID>::iterator it;
    Point_<T> v[3];
    for (it = triList.begin(); it != triList.end(); it++) {
        // Not interested in points outside the region.
        v[0] = vP[it->v[0]];
        v[1] = vP[it->v[1]];
        v[2] = vP[it->v[2]];

        cv::fillConvexPoly(mapMat, v, 3, it - triList.begin());
    }
}
